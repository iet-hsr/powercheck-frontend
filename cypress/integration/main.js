describe('The App', () => {
  before(() => {
    // runs once before all tests in the block
  });

  beforeEach(() => {
    // runs before each test in the block
  });

  it('successfully loads', () => {
    cy.visit('');
    cy.contains('PowerCheck');
  });

  it('handles missing base snapshot file as a failed spec', () => {
    if (Cypress.env('type') === 'actual') {
      try {
        cy.compareSnapshotTest('missing').should('be.false');
      } catch (e) {
        throw new Error('Missing snapshot file not handled correctly');
      }
    }
  });
});

describe('The Parameter Section', () => {
  before(() => {
    // runs once before all tests in the block
  });

  beforeEach(() => {
    // runs before each test in the block
    cy.visit('');
    cy.viewport(1280, 1500);
    cy.contains('PowerCheck');
  });

  describe('EndUser Section', () => {
    it('looks the same', () => {
      cy.get('button[aria-label="close"]')
        .click()
        .get('div[aria-controls="endUserbh-content"]')
        .click();
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(500);
      cy.get('div[id="endUserbh-content"]')
        .compareSnapshot('endUser');
    });
  });
});
