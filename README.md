[TOC]

# PowerCheck Frontend

## URLs

### Live (Production)

https://powercheck.ch

### Preview (Staging)

https://preview.powercheck.ch

## Repository

https://gitlab.com/iet-hsr/powercheck-frontend.git

## Passwords & Logins

All project related logins and passwords are stored in:

`...P_955_9998_Webenergierechner\Webenergierechner_2019\Zugangsdaten-DO-NOT-COMMIT.txt`

# Development Server Startup

This App consists of a React frontend App and an Express backend App. 

In a production environment the frontend gets build on server startup and is then delivered by the backend Express app.

In a development environment the frontend has its own webserver and doesn't rely on the backend to run (except for the API calls).

## Frontend

To start the frontend dev server, first navigate into the `frontend` folder:

```
cd frontend
```

Then start the dev server with:

```
npm run-script start
```

Runs on port 3000

# Used Frameworks and Libraries

## Express

Express is used for the backend API and to serve the frontend site.

## React

The frontend is a react app, bootstrapped with create react App. More infos further down.

## Internationalization 

The translation files are located here: `frontend/public/locales/`

`react-i18next`is used for text translations. More infos & documentation: https://react.i18next.com

## UI

Main UI-Library is `@material-ui`

## Icons

Custom Icons are converted with `@svgr/cli`:

- place the `.svg` icon in the folder `/resources/icons/`

- run `npm run svgr` and the icons get generated and saved into `/src/icons/`

- Use in components: 

```
react
import MyIcon from './icons/MyIcon';
<MyIcon width="40px" height="40px" />
```

## Feedback Mail - formspree.io

The feedback form gets sent to formspree.io From there it gets forwarded to whichever mail address is defined in the formspree.io settings.

To change the target mail address:

1. login to https://formspree.io ([see Logins & Passwords](#passwords-&-logins)) and open the settings for the `PowerCheck Feedback` Form. 
2. Change `Target Email` and click `save`.

