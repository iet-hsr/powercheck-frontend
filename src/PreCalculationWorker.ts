import _ from 'lodash';
import { DataSeries, IPreloadSecondaryDataCollection } from './domain_model/SourceDataCollection';
import { IPvOrientation } from './domain_model/Parameters';
import calculateSolarProduction from './business-logic/calculateSolarProduction';
import lowResReducer from './business-logic/lowResReducer';

export type PreCalculationInputDTO = {
  solarDirect: DataSeries;
  solarDiffuse: DataSeries;
  inputHash: string;
} & IPvOrientation & IPreloadSecondaryDataCollection;

export type PreCalculationOutputDTO = {
  inputHash: string;
  output: {
    highRes: DataSeries;
    lowRes: DataSeries;
    sum: number;
    max: number;
  };
}

// eslint-disable-next-line no-restricted-globals,@typescript-eslint/no-explicit-any
const ctx: Worker = self as any;


// eslint-disable-next-line no-restricted-globals
ctx.addEventListener(
  'message',
  ({ data }: { data: PreCalculationInputDTO }) => {
    // throw JSON.stringify(data);
    // eslint-disable-next-line no-restricted-globals
    const calculated = calculateSolarProduction(
      data.solarDirect,
      data.solarDiffuse,
      data.thetaA,
      data.thetaE,
      data.thetaZ,
      data.tilt,
      data.azimuth,
    );
    const returnMessage: PreCalculationOutputDTO = {
      inputHash: data.inputHash,
      output: {
        highRes: calculated,
        lowRes: calculated.reduce(lowResReducer, []),
        sum: _.sum(calculated),
        max: _.max(calculated) || 0,
      },
    };
    ctx.postMessage(returnMessage);
  },
  false,
);
