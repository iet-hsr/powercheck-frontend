import React from "react";

const SvgCh = props => (
  <svg viewBox="0 0 200 115.3" {...props}>
    <path d="M187.7 49.3H173l-11-9v-16L136-1.9h-14.4l-9 6.2H74L62.3 19.4H45L22 43.3l-.4 13.4L0 78.3v16h20l12.5-12.5H50v15.5l16 16h46.5l16.5-17 7 6v12h16v-10l15-16 7.5 6.5H185V72.3h14v-11l-11.3-12zM140 65.4h-26.6V92H90.8V65.4H64.3V42.8h26.6V16.3h22.6v26.6H140v22.5z" />
  </svg>
);

export default SvgCh;
