import React from 'react';
import { useTranslation } from 'react-i18next';
import YearAndScaling from '../input-combined/YearAndScaling';
import Quantity from '../../../../domain_model/math/Quantity';
import { DB_POWER_UNIT } from '../../../../domain_model/math/PowerUnit';
import { ClickableInfoButtonProps } from '../../../shared/ClickableInfoButton';

const GRAPH_LIMIT = new Quantity(10, DB_POWER_UNIT, false);

const RiverParams = React.memo(() => {
  const { t } = useTranslation();

  const infoButtonProps: ClickableInfoButtonProps = {
    dokuPage: 'user-manual.html',
    paragraph: {
      de: 'flusskraftwerke',
      // TODO: add en doku paragraph
      en: 'flusskraftwerke',
    },
  };

  return (
    <YearAndScaling
      section="river"
      scalingLabel={t('paramsMaxPower')}
      graphLimit={GRAPH_LIMIT}
      originalUnit={DB_POWER_UNIT}
      infoButtonProps={infoButtonProps}
      yearTooltip={t('infoRiverYear')}
      scalingTootltip={t('infoRiverScaling')}
    />
  );
});

export default RiverParams;
