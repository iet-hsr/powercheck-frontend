import React, { FunctionComponent } from 'react';
import {
  createStyles,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import clsx from 'clsx';

export type Row = string[];

type Props = {
  headerRow: Row;
  rows: Row[];
}

const useStyles = makeStyles(() => createStyles({
  table: {
    marginTop: '-0.5em',
  },
  titleCell: {
    fontSize: '0.8rem',
    fontWeight: 600,
    lineHeight: 1.2,
    height: '2em',
  },
  textCell: {
    fontSize: '0.8rem',
  },
  superSmallCell: {
    padding: '0px 24px 0px 16px',
  },
}));


const InfoButtonTable: FunctionComponent<Props> = (props) => {
  const { headerRow, rows } = props;
  const classes = useStyles();

  return (
    <Table size="small" className={classes.table}>
      <TableHead>
        <TableRow>
          {
            headerRow.map((cell, i) => (
              <TableCell
                align={i === 0 ? 'left' : 'right'}
                className={classes.titleCell}
                key={`header${i}`}
              >
                {cell}
              </TableCell>
            ))
          }
        </TableRow>
      </TableHead>
      <TableBody>
        {
          rows.map((row, i) => (
            <TableRow key={`row${i}`}>
              {
                row.map((cell, j) => (
                  <TableCell
                    align={j === 0 ? 'left' : 'right'}
                    className={clsx(
                      classes.superSmallCell,
                      j === 0 ? classes.titleCell : classes.textCell,
                    )}
                    key={`row${i}col${j}`}
                  >
                    {cell}
                  </TableCell>
                ))
              }
            </TableRow>
          ))
        }
      </TableBody>
    </Table>
  );
};
export default InfoButtonTable;
