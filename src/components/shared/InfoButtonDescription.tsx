import React, { FunctionComponent } from 'react';
import {
  createStyles, makeStyles, Typography,
} from '@material-ui/core';

type Props = {
  title?: string;
  italic?: boolean;
}

const useStyles = makeStyles(() => createStyles({
  title: {
    fontSize: '0.9rem',
    paddingBottom: '0.2em',
    fontStyle: ({ italic }: Props) => (italic ? 'italic' : 'normal'),
  },
  text: {
    fontSize: '0.8rem',
    paddingBottom: '1.5em',
  },
}));

const InfoButtonDescription: FunctionComponent<Props> = (props) => {
  const classes = useStyles(props);
  const { children, title } = props;

  return (
    <>
      {title && (
        <Typography variant="subtitle2" className={classes.title}>
          {title}
        </Typography>
      )}
      {children && (
        <Typography variant="body2" className={classes.text}>
          {children}
        </Typography>
      )}
    </>
  );
};

export default InfoButtonDescription;
