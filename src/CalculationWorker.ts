import calculate from './business-logic/calculate';
import SourceDataCollection, {
  DataSeries,
  IPreloadDataCollection,
  SourceDataCollectionDTO,
} from './domain_model/SourceDataCollection';
import { IParameterCollection } from './domain_model/Parameters';

export type CalculationInputDTO = {
  srcData: SourceDataCollectionDTO;
  params: IParameterCollection;
  srcPreloadData: IPreloadDataCollection;
  solarPreCalc: DataSeries;
}

// eslint-disable-next-line no-restricted-globals,@typescript-eslint/no-explicit-any
const ctx: Worker = self as any;


// eslint-disable-next-line no-restricted-globals
ctx.addEventListener(
  'message',
  ({ data }: { data: CalculationInputDTO }) => {
    // throw JSON.stringify(data);
    // eslint-disable-next-line no-restricted-globals
    ctx.postMessage(
      calculate({
        srcData: new SourceDataCollection(undefined, data.srcData),
        params: data.params,
        srcPreloadData: data.srcPreloadData,
        solarPreCalc: data.solarPreCalc,
      }),
    );
  },
  false,
);
