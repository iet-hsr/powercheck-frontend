import { ComputedActionTypes, IComputedState } from './types/computedTypes';
import { DataActionTypes, IDataState } from './types/dataTypes';
import { ISizeState, IUnitState, MiscActionTypes } from './types/miscTypes';
import { IParameterState, ParameterActionTypes } from './types/parameterTypes';
import { IUiState, UiActionTypes } from './types/uiTypes';


export interface IReduxState {
  size: ISizeState;
  unit: IUnitState;
  uiState: IUiState;
  data: IDataState;
  computed: IComputedState;
  parameter: IParameterState;
}

export interface IControllerState extends IUiState, IDataState, IParameterState, IComputedState {}

export type ActionTypes = (
  MiscActionTypes
  | UiActionTypes
  | DataActionTypes
  | ComputedActionTypes
  | ParameterActionTypes
)
