export const getCustomDataStatsQuo = () => ({
  customConsumption: [],
  customProduction: [],
});

export const getStatusQuoParams = () => ({
  endUser: {
    year: 2016,
    scaling: 56669.22999999995,
  },
  nuclear: {
    year: 2016,
    scaling: 2.97,
  },
  thermal: {
    year: 2016,
    scaling: 0.7,
  },
  solar: {
    year: 2016,
    scaling: 1.9,
  },
  wind: {
    year: 2016,
    scaling: 0.06,
  },
  river: {
    year: 2016,
    scaling: 3.29,
  },
  dam: {
    year: 2016,
    outputPower: 8.15,
    outputEfficiency: 0.8,
    maxCapacity: 8833,
    initialCapacity: 4065.81,
  },
  pumpStorage: {
    intakePower: 2.56,
    outputPower: 2.56,
    maxCapacity: 200,
    initialCapacity: 92.04,
    outputEfficiency: 0.75,
    inputEfficiency: 0.7,
  },
  batteryStorage: {
    intakePower: 0.01,
    outputPower: 0.01,
    maxCapacity: 0.01,
    initialCapacity: 0.01,
    outputEfficiency: 0.9,
    inputEfficiency: 0.8,
  },
  customConsumption: [],
  customProduction: [],
});
