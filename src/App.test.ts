import _ from 'lodash';
import { dischargeDamOverflow } from './business-logic/storage/dischargeDamOverflow';
import { toEnergy, toPower } from './domain_model/math/helpers';
import { StorageState } from './business-logic/types';


// it('calculates storage as before', () => {
//   const params = statusQuoParams;
//   const loads = JSON.parse(statusQuoLoadsString);
//   const customData = customDataStatsQuo;
//   const storage = calculateStorage(params, loads, customData);
//   const storageNormalized = JSON.parse(JSON.stringify(storage));
//   const expectedNormalized = JSON.parse(calculateStorageResultString);
//
//   const check = (type, fn) => {
//     const checkList = storageNormalized[type].map(
//       (x, i) => fn(x - expectedNormalized[type][i])
//         || `${type}, ${i}: Diff of ${x - expectedNormalized[type][i]}`,
//     );
//     checkList.map((x) => expect(x).toEqual(true));
//   };
//
//   check('residual', (x) => Math.abs(x) < 0.0000001);
//   check('damPower', (x) => Math.abs(x) < 0.0000001);
//   check('damStorageLevel', (x) => x > -0.0000001);
//   check('damStorageLevelIncrease', (x) => x > -0.01);
//   check('damStorageLevelDecrease', (x) => Math.abs(x) < 0.0000001);
//
//   check('pumpPower', (x) => Math.abs(x) < 0.0000001);
//   check('pumpStorageLevel', (x) => Math.abs(x) < 0.0000001);
//   check('pumpStorageLevelIncrease', (x) => Math.abs(x) < 0.0000001);
//   check('pumpStorageLevelDecrease', (x) => Math.abs(x) < 0.0000001);
//
//   check('batteryPower', (x) => Math.abs(x) < 0.0000001);
//   check('batteryStorageLevel', (x) => Math.abs(x) < 0.0000001);
//   check('batteryStorageLevelIncrease', (x) => Math.abs(x) < 0.0000001);
//   check('batteryStorageLevelDecrease', (x) => Math.abs(x) < 0.0000001);
//
//   // expect(storageNormalized).toEqual(expectedNormalized);
// });


// it('calculates storage with plausible results', () => {
//   const params = getStatusQuoParams();
//   const loads = getStatusQuoLoads();
//   const customData = getCustomDataStatsQuo();
//   const storage = calculateStorage(params, loads, customData);
//
//   const check = (type, fn) => {
//     const checkList = storage[type].map(
//       (x, i) => fn(x)
//         || `${type}, ${i}: ${x}`,
//     );
//     checkList.map((x) => expect(x).toBe(true));
//   };
//
//   check('residual', (x) => Math.abs(x) < 100);
//   check('damPower', (x) => Math.abs(x) < 10);
//   check('damStorageLevel', (x) => x >= 0 && x <= params.dam.maxCapacity);
//   check('damStorageLevelIncrease', (x) => x >= 0);
//   check('damStorageLevelDecrease', (x) => x >= 0);
//
//   check('pumpPower', (x) => Math.abs(x) < 10);
//   check('pumpStorageLevel', (x) => x >= 0 && x <= params.pumpStorage.maxCapacity);
//   check('pumpStorageLevelIncrease', (x) => x >= 0);
//   check('pumpStorageLevelDecrease', (x) => x >= 0);
//
//   check('batteryPower', (x) => Math.abs(x) < 1);
//   check('batteryStorageLevel', (x) => x >= 0 && x <= params.batteryStorage.maxCapacity);
//   check('batteryStorageLevelIncrease', (x) => x >= 0);
//   check('batteryStorageLevelDecrease', (x) => x >= 0);
// });

it('discharges small dam overflow correctly', () => {
  const overflow = 1;

  const state: StorageState = {
    damOverflow: 0,
    levels: {
      dam: 8833 + overflow,
      pump: 190,
      battery: 0.01,
    },
    residual: 0,
    consumedPower: {
      pump: 0,
      battery: 0,
    },
    producedPower: {
      dam: 0,
      pump: 0,
      battery: 0,
    },
  };
  const dischargePowers = {
    dam: 8.15,
    pump: 2.56,
    battery: 0.01,
  };
  const efficiencies = {
    dam: 0.8,
    pump: 0.75,
    battery: 0.9,
  };
  const capacities = {
    dam: 8833,
    pump: 200,
    battery: 0.01,
  };

  const expectedState = _.cloneDeep(state);
  expectedState.producedPower.dam = toPower(overflow) * efficiencies.dam;
  expectedState.residual = toPower(overflow) * efficiencies.dam;
  expectedState.levels.dam = state.levels.dam - overflow;

  const result = dischargeDamOverflow(state, dischargePowers, efficiencies, capacities);

  expect(result).toEqual(expectedState);
});


it('discharges massive dam overflow correctly', () => {
  const overflow = 10;

  const state: StorageState = {
    damOverflow: 0,
    levels: {
      dam: 8833 + overflow,
      pump: 190,
      battery: 0.01,
    },
    residual: 0,
    producedPower: {
      dam: 0,
      pump: 0,
      battery: 0,
    },
    consumedPower: {
      pump: 0,
      battery: 0,
    },
  };
  const dischargePowers = {
    dam: 8.15,
    pump: 2.56,
    battery: 0.01,
  };
  const efficiencies = {
    dam: 0.8,
    pump: 0.75,
    battery: 0.9,
  };
  const capacities = {
    dam: 8833,
    pump: 200,
    battery: 0.01,
  };

  const expectedState = _.cloneDeep(state);
  expectedState.producedPower.dam = dischargePowers.dam;
  expectedState.residual = dischargePowers.dam;
  expectedState.levels.dam = state.levels.dam - toEnergy(dischargePowers.dam) / efficiencies.dam;

  const result = dischargeDamOverflow(state, dischargePowers, efficiencies, capacities);

  expect(result).toEqual(expectedState);
});
