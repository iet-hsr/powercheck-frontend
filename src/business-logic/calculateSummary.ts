import * as _ from 'lodash';
import {
  IComputedMisc,
  IComputedStorage,
  IComputedSummary,
  ICustomData,
  ILoads,
} from '../domain_model/ComputedDataCollection';
import { toEnergy } from '../domain_model/math/helpers';
import { IParameterCollection } from '../domain_model/Parameters';
import { DATA_POINT_DURATION_H } from './constants';

const sumUpEnergy = (array: number[]) => toEnergy(_.sum(array));

const getFirstLastDiff = (first: number, lastArray: number[]) => (_.last(lastArray) || 0) - first;

const calculateSummary = (
  storage: IComputedStorage,
  loads: ILoads,
  misc: IComputedMisc,
  custom: ICustomData,
  params: IParameterCollection,
): IComputedSummary => {
  const {
    damProducedPower,
    damStorageLevel,
    damOverflow,
    pumpProducedPower,
    pumpConsumedPower,
    pumpStorageLevel,
    batteryProducedPower,
    batteryConsumedPower,
    batteryStorageLevel,
  } = storage;
  const {
    basicLoads: {
      nuclear, river, solar, thermal, wind, endUser, export: exportLoad, import: importLoad,
    },
    specialLoads: { networkLosses, damInflux },
  } = loads;
  const {
    totalConsumption,
    totalProduction,
    excessSeries,
    shortageSeries,
  } = misc;
  const { customConsumption, customProduction } = custom;
  const {
    waterStorage: {
      outputEfficiency: waterStorageOutputEfficiency,
      intakeEfficiency: waterStorageIntakeEfficiency,
      dam: {
        initialUsableCapacity: damInitialUsableCapacity,
      },
      pumpStorage: {
        initialUsableCapacity: pumpInitialUsableCapacity,
      },
    },
    batteryStorage: {
      outputEfficiency: batteryOutputEfficiency,
      initialUsableCapacity: batteryInitialUsableCapacity,
      intakeEfficiency: batteryIntakeEfficiency,
    },
  } = params;

  const damInitialCapacity = damInitialUsableCapacity / waterStorageOutputEfficiency;
  const pumpInitialCapacity = pumpInitialUsableCapacity / waterStorageOutputEfficiency;
  const batteryInitialCapacity = batteryInitialUsableCapacity / batteryOutputEfficiency;

  const totalEnergyConsumption = sumUpEnergy(totalConsumption);
  const totalEnergyProduction = sumUpEnergy(totalProduction);

  const endUserEnergyConsumption = sumUpEnergy(endUser);
  const networkLossEnergyConsumption = sumUpEnergy(networkLosses);

  const exportEnergy = sumUpEnergy(exportLoad);
  const importEnergy = sumUpEnergy(importLoad);
  const nettImportEnergy = importEnergy - exportEnergy;

  const excessEnergy = sumUpEnergy(excessSeries);
  const shortageEnergy = sumUpEnergy(shortageSeries);
  const nettShortageEnergy = shortageEnergy - excessEnergy;

  const totalEnergyProductionPlusImportAndShortage = totalEnergyProduction
    + shortageEnergy + importEnergy;

  const totalEnergyConsumptionPlusExport = totalEnergyConsumption
    + exportEnergy;

  const nuclearEnergyProduction = sumUpEnergy(nuclear);
  const thermalEnergyProduction = sumUpEnergy(thermal);
  const solarEnergyProduction = sumUpEnergy(solar);
  const windEnergyProduction = sumUpEnergy(wind);
  const riverEnergyProduction = sumUpEnergy(river);
  const damEnergyProduction = sumUpEnergy(damProducedPower);
  const damEnergyOverflow = sumUpEnergy(damOverflow);
  const damEnergyInflux = sumUpEnergy(damInflux);

  const pumpEnergyProduction = sumUpEnergy(pumpProducedPower);
  const pumpEnergyConsumption = sumUpEnergy(pumpConsumedPower);
  const pumpEnergyNettProduction = pumpEnergyProduction - pumpEnergyConsumption;

  const batteryEnergyProduction = sumUpEnergy(batteryProducedPower);
  const batteryEnergyConsumption = sumUpEnergy(batteryConsumedPower);
  const batteryEnergyNettProduction = batteryEnergyProduction - batteryEnergyConsumption;

  const customEnergyProduction = sumUpEnergy(customProduction);
  const customEnergyConsumption = sumUpEnergy(customConsumption);
  const customEnergyNettProduction = customEnergyProduction - customEnergyConsumption;

  const usableStorageDifference = (
    getFirstLastDiff(damInitialCapacity, damStorageLevel) * waterStorageOutputEfficiency
    + getFirstLastDiff(pumpInitialCapacity, pumpStorageLevel) * waterStorageOutputEfficiency
    + getFirstLastDiff(batteryInitialCapacity, batteryStorageLevel) * batteryOutputEfficiency
  );
  const usableInitialStorageLevel = damInitialUsableCapacity
    + pumpInitialUsableCapacity
    + batteryInitialUsableCapacity;
  const usableFinalStorageLevel = usableInitialStorageLevel + usableStorageDifference;

  const damUsableStorageLevelEnergyIncrease = (damEnergyInflux - damEnergyOverflow)
    * waterStorageOutputEfficiency;
  const damUsableStorageLevelEnergyDecrease = damEnergyProduction;

  const pumpUsableStorageLevelEnergyIncrease = pumpEnergyConsumption
    * waterStorageOutputEfficiency * waterStorageIntakeEfficiency;
  const pumpUsableStorageLevelEnergyDecrease = pumpEnergyProduction;

  const batteryUsableStorageLevelEnergyDecrease = batteryEnergyConsumption
    * batteryOutputEfficiency * batteryIntakeEfficiency;
  const batteryUsableStorageLevelEnergyIncrease = batteryEnergyProduction;

  const damUsableStorageLevelEnergyDiff = damUsableStorageLevelEnergyIncrease
    - damUsableStorageLevelEnergyDecrease;
  const pumpUsableStorageLevelEnergyDiff = pumpUsableStorageLevelEnergyIncrease
    - pumpUsableStorageLevelEnergyDecrease;
  const batteryUsableStorageLevelEnergyDiff = batteryUsableStorageLevelEnergyIncrease
    - batteryUsableStorageLevelEnergyDecrease;

  const totalConsumptionEnergy = sumUpEnergy(totalConsumption);

  const selfSufficiencyPercentage = 100 - (
    ((shortageEnergy + importEnergy - Math.min(0, usableStorageDifference))
      / totalConsumptionEnergy
    ) * 100);
  const importDurationH = _.zip(shortageSeries, importLoad)
    .filter(([a, b]) => a !== 0 || b !== 0)
    .length
    * DATA_POINT_DURATION_H;
  const importMaxPower = _.max(_.zipWith(shortageSeries, importLoad, (a, b) => a + b)) || 0;

  return {
    totalEnergyConsumption,
    totalEnergyConsumptionPlusExport,
    totalEnergyProduction,
    totalEnergyProductionPlusImportAndShortage,

    importEnergy,
    exportEnergy,
    nettImportEnergy,

    excessEnergy,
    shortageEnergy,
    nettShortageEnergy,
    selfSufficiencyPercentage,
    importDurationH,
    importMaxPower,

    nuclearEnergyProduction,
    thermalEnergyProduction,
    solarEnergyProduction,
    windEnergyProduction,
    riverEnergyProduction,
    damEnergyProduction,

    pumpEnergyProduction,
    pumpEnergyConsumption,
    pumpEnergyNettProduction,

    batteryEnergyProduction,
    batteryEnergyConsumption,
    batteryEnergyNettProduction,

    customEnergyProduction,
    customEnergyConsumption,
    customEnergyNettProduction,

    endUserEnergyConsumption,
    networkLossEnergyConsumption,

    usableStorageDifference,
    usableFinalStorageLevel,
    damUsableStorageLevelEnergyDiff,
    damUsableStorageLevelEnergyIncrease,
    damUsableStorageLevelEnergyDecrease,
    pumpUsableStorageLevelEnergyDiff,
    pumpUsableStorageLevelEnergyIncrease,
    pumpUsableStorageLevelEnergyDecrease,
    batteryUsableStorageLevelEnergyDiff,
    batteryUsableStorageLevelEnergyIncrease,
    batteryUsableStorageLevelEnergyDecrease,
  };
};

export default calculateSummary;
