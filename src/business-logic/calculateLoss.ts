import _ from 'lodash';
import { DataSeries } from '../domain_model/SourceDataCollection';
import { IParameterCollection } from '../domain_model/Parameters';
import { ICustomData, ILoads } from '../domain_model/ComputedDataCollection';


const calculateLoss = (
  loads: ILoads,
  customData: ICustomData,
  params: IParameterCollection,
): DataSeries => {
  const { basicLoads: { endUser } } = loads;
  const { customConsumption } = customData;
  const f = params.network.lossFactor;

  return _.zip(endUser, customConsumption).map(
    ([endUserVal, customConsumptionVal]) => (
      ((endUserVal || 0) + (customConsumptionVal || 0)) * (f / (1 - f))
    ),
  );
};

export default calculateLoss;
