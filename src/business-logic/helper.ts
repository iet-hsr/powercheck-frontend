export const roundTo = (x: number, decimalPlaces: number): number => (
  Math.round((x + Number.EPSILON) * (10 ** decimalPlaces))
  / (10 ** decimalPlaces)
);
