import _ from 'lodash';
import { DataSeries } from '../domain_model/SourceDataCollection';
import { WeightedAngle } from '../domain_model/Parameters';

/**
 * Calculate Solar Production
 * @param solarDirect
 * @param solarDiffuse
 * @param thetaA Sun direction or azimuth (Himmelsrichtung)
 * @param thetaE Sun elevation angle (Neigungswinkel vom Horizont aus)
 * @param thetaZ Sun zenith angle (Neigungswinkel vom höchsten punkt aus)
 * @param betaParams PV Panel Tilt weighted angles (Gewichtete Neigungswinkel)
 * @param phiParams PV Panel weighted Direction or azimuth (Gewichtete Himmelsrichtungen)
 */
function calculateSolarProduction(
  solarDirect: DataSeries,
  solarDiffuse: DataSeries,
  thetaA: DataSeries,
  thetaE: DataSeries,
  thetaZ: DataSeries,
  betaParams: WeightedAngle[],
  phiParams: WeightedAngle[],
): number[] {
  let IrradiationDirectTilt = new Array(365 * 24 * 4).fill(0);
  let IrradiationDiffuseTilt = new Array(365 * 24 * 4).fill(0);

  const betaWeightTotal = _.sum(betaParams.map((p) => p.weight));
  const phiWeightTotal = _.sum(phiParams.map((p) => p.weight));

  betaParams.forEach(({ angle: betaAngle, weight: betaWeightAbsolute }) => {
    const betaWeight = betaWeightAbsolute / betaWeightTotal;

    // I_diffus_t_Incr =
    //   SolarData15(locNr).Diffusstrahlung(index)
    //   * (1 + cos(beta.angle(j) / 180*pi))
    //   / 2; % independent of Phi
    const IrradiationDiffuseTiltIncrement = solarDiffuse.map((x) => (
      (x * (1 + Math.cos((betaAngle / 180) * Math.PI))) / 2
    ));

    IrradiationDiffuseTilt = IrradiationDiffuseTilt.map((x, i) => (
      x + (betaWeight * IrradiationDiffuseTiltIncrement[i])
    ));

    phiParams.forEach(({ angle: phiAngle, weight: phiWeightAbsolute }) => {
      const phiWeight = phiWeightAbsolute / phiWeightTotal;

      const conversionFactorVector = createConversionFactorVector(
        betaAngle, phiAngle, thetaA, thetaE, thetaZ,
      );
      const IrradiationDirectTiltIncrement = solarDirect.map((x, i) => (
        x * conversionFactorVector[i]
      ));
      IrradiationDirectTilt = IrradiationDirectTilt.map(
        (x, i) => x + betaWeight * phiWeight * IrradiationDirectTiltIncrement[i],
      );
    });
  });

  // solarProd = IrradiationTilt; Es fehlt noch power factor...
  return IrradiationDirectTilt.map((x, i) => (
    ((x + IrradiationDiffuseTilt[i])
      * 0.705) // Add XXX Factor
    / 1000)); // Convert to GW
}


/**
 * Creates conversion factor vector, used for calculation of direct irradiation of tilted pv modules
 * from horizontal direct irradiation data
 * @param betaAngle PV Panel Tilt angle (Neigungswinkel)
 * @param phiAngle PV Panel Direction or azimuth (Himmelsrichtung)
 * @param thetaA Sun direction or azimuth (Himmelsrichtung)
 * @param thetaE Sun elevation angle (Neigungswinkel vom Horizont aus)
 * @param thetaZ Sun zenith angle (Neigungswinkel vom höchsten punkt aus)
 */
function createConversionFactorVector(
  betaAngle: number, phiAngle: number,
  thetaA: number[], thetaE: number[], thetaZ: number[],
) {
  // convVec has maximum factor, if not there are big multiplications factor at sunrise and sunset
  const conversionMax = 10;

  // cos_Theta_i =
  //  sin(Theta_e/180*pi) .* cos(beta/180*pi)
  //  + cos(Theta_e/180*pi) .* sin(beta/180*pi) .* cos((Phi-Theta_a)/180*pi);
  const cosThetaI = thetaE.map((x, i) => (
    Math.sin((x / 180) * Math.PI) * Math.cos((betaAngle / 180) * Math.PI)
    + (
      Math.cos((x / 180) * Math.PI) * Math.sin((betaAngle / 180) * Math.PI)
      * Math.cos(((phiAngle - thetaA[i]) / 180) * Math.PI)
    )
  ));

  // convVec = cos_Theta_i ./ cos(Theta_z/180*pi);
  let conversionVector = cosThetaI.map((x, i) => (
    x / Math.cos((thetaZ[i] / 180) * Math.PI)
  ));

  // convVec(Theta_e<0) = 0;    % convVec is zero when the sun is down (Theta_e < 0°)
  conversionVector = conversionVector.map((x, i) => (
    thetaE[i] < 0 ? 0 : x
  ));

  // convVec(convVec<0) = 0;
  conversionVector = conversionVector.map((x) => (
    Math.max(0, x)
  ));

  // convVec(convVec>convMax) = convMax;
  conversionVector = conversionVector.map((x) => (
    Math.min(x, conversionMax)
  ));

  return conversionVector;
}

export default calculateSolarProduction;
