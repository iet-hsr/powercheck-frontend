import * as _ from 'lodash';
import {
  IComputedMisc,
  IComputedStorage,
  ICustomData,
  ILoads,
} from '../domain_model/ComputedDataCollection';
import { roundTo } from './helper';

const invert = (x: number) => -x;

const calculateMisc = (
  storage: IComputedStorage,
  loads: ILoads,
  customData: ICustomData,
): IComputedMisc => {
  const {
    damProducedPower,
    pumpConsumedPower, pumpProducedPower,
    batteryConsumedPower, batteryProducedPower,
  } = storage;
  const {
    basicLoads: {
      endUser, nuclear, river, solar, thermal, wind, import: importLoad, export: exportLoad,
    }, specialLoads: { networkLosses },
  } = loads;
  const { customConsumption, customProduction } = customData;

  const damNegativeProducedPower = damProducedPower.map(invert);
  const pumpNegativeProducedPower = pumpProducedPower.map(invert);
  const batteryNegativeProducedPower = batteryProducedPower.map(invert);

  const dataLength = Math.min(
    nuclear.length,
    thermal.length,
    solar.length,
    wind.length,
    river.length,
    damProducedPower.length,
    pumpProducedPower.length,
    batteryProducedPower.length,
  );

  const totalConsumption = new Array(dataLength).fill(0).map((__, index) => (
    endUser[index]
    + networkLosses[index]
    + (customConsumption[index] || 0)
    + pumpConsumedPower[index]
    + batteryConsumedPower[index]
  ));

  const totalConsumptionPlusExport = new Array(dataLength).fill(0).map((__, index) => (
    totalConsumption[index]
    + exportLoad[index]
  ));

  const totalProduction = new Array(dataLength).fill(0).map((__, index) => (
    nuclear[index]
    + thermal[index]
    + solar[index]
    + wind[index]
    + river[index]
    + damProducedPower[index]
    + pumpProducedPower[index]
    + batteryProducedPower[index]
    + (customProduction[index] || 0)
  ));

  const totalProductionPlusImport = new Array(dataLength).fill(0).map((__, index) => (
    totalProduction[index]
    + importLoad[index]
  ));

  const excessSeries = _.zipWith(totalProductionPlusImport, totalConsumptionPlusExport, (
    production: number | undefined,
    consumption: number | undefined,
  ) => (
    roundTo(
      Math.max((production || 0) - (consumption || 0), 0),
      3,
    )
  ));

  const shortageSeries = _.zipWith(
    totalConsumptionPlusExport,
    totalProductionPlusImport,
    (
      consumption: number | undefined,
      production: number | undefined,
    ) => (
      roundTo(
        Math.max((consumption || 0) - (production || 0), 0),
        3,
      )
    ),
  );

  return {
    damNegativeProducedPower,
    pumpNegativeProducedPower,
    batteryNegativeProducedPower,

    totalConsumption,
    totalProduction,

    excessSeries,
    shortageSeries,
  };
};

export default calculateMisc;
