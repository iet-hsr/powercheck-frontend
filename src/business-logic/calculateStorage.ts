import { IParameterCollection } from '../domain_model/Parameters';
import { IComputedStorage, ICustomData, ILoads } from '../domain_model/ComputedDataCollection';
import {
  IStorageChargePowers, IStorageLevels, IStoragePowers, StorageState,
} from './types';
import { calculateResidual } from './storage/residual';
import { toEnergy } from '../domain_model/math/helpers';
import { charge } from './storage/charge';
import { discharge } from './storage/discharge';
import { dischargeDamOverflow } from './storage/dischargeDamOverflow';
import { dischargeMinimalPower } from './storage/dischargeMinimalPower';


// Caution, this function mutates the loads parameter's specialLoads.networkLosses
function calculateStorage(
  params: IParameterCollection,
  loads: ILoads,
  customData: ICustomData,
): IComputedStorage {
  const {
    batteryStorage: {
      intakePowerInHours: batteryChargePowerInHours,
      outputPowerInHours: batteryDischargePowerInHours,
      intakeEfficiency: batteryIntakeEfficiency,
      outputEfficiency: batteryOutputEfficiency,
      maxUsableCapacity: batteryUsableCapacity,
      initialUsableCapacity: batteryInitUsableCapacity,
    },
    waterStorage: {
      intakeEfficiency: waterStorageIntakeEfficiency,
      outputEfficiency: waterStorageOutputEfficiency,
      pumpStorage: {
        intakePower: pumpChargePower,
        outputPower: pumpDischargePower,
        maxUsableCapacity: pumpUsableCapacity,
        initialUsableCapacity: pumpInitUsableCapacity,
      },
      dam: {
        outputPower: damDischargePower,
        maxUsableCapacity: damUsableCapacity,
        initialUsableCapacity: damInitUsableCapacity,
        minimalPower: damMinimalPower,
      },
    },
  } = params;

  const {
    basicLoads: {
      endUser: endUserLoad,
      import: importLoad,
      export: exportLoad,
      solar: solarLoad,
      thermal: thermalLoad,
      nuclear: nuclearLoad,
      river: riverLoad,
      wind: windLoad,
    },
    specialLoads: {
      damInflux,
    },
  } = loads;

  const capacities = {
    battery: batteryUsableCapacity / batteryOutputEfficiency,
    pump: pumpUsableCapacity / waterStorageOutputEfficiency,
    dam: damUsableCapacity / waterStorageOutputEfficiency,
  };

  const dischargePowers = {
    battery: capacities.battery / batteryDischargePowerInHours,
    pump: pumpDischargePower,
    dam: damDischargePower,
  };

  const chargePowers = {
    battery: capacities.battery / batteryChargePowerInHours,
    pump: pumpChargePower,
  };

  const intakeEfficiencies = {
    battery: batteryIntakeEfficiency,
    pump: waterStorageIntakeEfficiency,
  };

  const dischargeEfficiencies = {
    battery: batteryOutputEfficiency,
    pump: waterStorageOutputEfficiency,
    dam: waterStorageOutputEfficiency,
  };

  const dataLength = Math.min(
    endUserLoad.length,
    importLoad.length,
    exportLoad.length,
    nuclearLoad.length,
    thermalLoad.length,
    solarLoad.length,
    windLoad.length,
    riverLoad.length,
    damInflux.length,
  );

  const result: IComputedStorage = {
    residual: [],
    damProducedPower: [],
    damUsableStorageLevel: [],
    damStorageLevel: [],
    damOverflow: [],
    pumpConsumedPower: [],
    pumpProducedPower: [],
    pumpUsableStorageLevel: [],
    pumpStorageLevel: [],
    batteryConsumedPower: [],
    batteryProducedPower: [],
    batteryUsableStorageLevel: [],
    batteryStorageLevel: [],
  };

  function addToResult(
    index: number,
    {
      residual, levels, consumedPower, producedPower, damOverflow,
    }: {
      levels: IStorageLevels;
      residual: number;
      consumedPower: IStorageChargePowers;
      producedPower: IStoragePowers;
      damOverflow: number;
    },
  ) {
    result.residual[index] = residual;
    result.damProducedPower[index] = producedPower.dam;
    result.damOverflow[index] = damOverflow;
    result.pumpConsumedPower[index] = consumedPower.pump;
    result.pumpProducedPower[index] = producedPower.pump;
    result.batteryConsumedPower[index] = consumedPower.battery;
    result.batteryProducedPower[index] = producedPower.battery;
    result.damStorageLevel[index] = levels.dam;
    result.damUsableStorageLevel[index] = levels.dam * waterStorageOutputEfficiency;
    result.pumpStorageLevel[index] = levels.pump;
    result.pumpUsableStorageLevel[index] = levels.pump * waterStorageOutputEfficiency;
    result.batteryStorageLevel[index] = levels.battery;
    result.batteryUsableStorageLevel[index] = levels.battery * batteryOutputEfficiency;
  }

  const lastLevels = {
    dam: damInitUsableCapacity / waterStorageOutputEfficiency,
    battery: batteryInitUsableCapacity / batteryOutputEfficiency,
    pump: pumpInitUsableCapacity / waterStorageOutputEfficiency,
  };

  for (let index = 0; index < dataLength; index += 1) {
    let state: StorageState = {
      residual: calculateResidual(index, loads, customData),
      levels: lastLevels,
      consumedPower: {
        battery: 0, pump: 0,
      },
      producedPower: {
        battery: 0, dam: 0, pump: 0,
      },
      damOverflow: 0,
    };

    // Add Dam Influx to dam level, ignoring the max capacity

    state.levels.dam += Math.max(0, toEnergy(damInflux[index]));


    // Handle Dam Overflow

    state.damOverflow = Math.max(0, state.levels.dam - capacities.dam);
    if (state.damOverflow > 0) {
      // discharge dam storage
      state = dischargeDamOverflow(
        state,
        dischargePowers,
        dischargeEfficiencies,
        capacities,
      );
      state.damOverflow = Math.max(0, state.levels.dam - capacities.dam);

      // Cap dam level to max capacity
      state.levels.dam -= state.damOverflow;
    }

    // Discharge based on minimalPower parameter

    state = dischargeMinimalPower(
      state,
      { dam: damMinimalPower },
      dischargeEfficiencies,
    );

    // Charge / Discharge based on residual

    if (state.residual > 0) {
      // Overproduction
      state = charge(state, chargePowers, intakeEfficiencies, capacities);
    } else if (state.residual < 0) {
      // Underproduction
      state = discharge(
        state, dischargePowers, dischargeEfficiencies,
      );
    }

    addToResult(
      index,
      state,
    );

    lastLevels.dam = state.levels.dam;
    lastLevels.battery = state.levels.battery;
    lastLevels.pump = state.levels.pump;
  }
  return result;
}

export default calculateStorage;
