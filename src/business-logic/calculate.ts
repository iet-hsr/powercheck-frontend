import { IComputedDataWithoutInfo } from '../domain_model/ComputedDataCollection';
import calculateLoads from './calculateLoads';
import SourceDataCollection, {
  DataSeries,
  IPreloadDataCollection,
} from '../domain_model/SourceDataCollection';
import { IParameterCollection } from '../domain_model/Parameters';
import calculateStorage from './calculateStorage';
import calculateMisc from './calculateMisc';
import calculateSummary from './calculateSummary';
import calculateCustom from './calculateCustom';
import calculateLoss from './calculateLoss';

export type CalculationInput = {
  srcData: SourceDataCollection;
  params: IParameterCollection;
  srcPreloadData: IPreloadDataCollection;
  solarPreCalc: DataSeries;
}

function calculate(
  {
    srcData,
    params,
    srcPreloadData,
    solarPreCalc,
  }: CalculationInput,
): IComputedDataWithoutInfo {
  const loads = calculateLoads(srcData, srcPreloadData, params, solarPreCalc);
  const customData = calculateCustom(params);
  loads.specialLoads.networkLosses = calculateLoss(loads, customData, params);
  const storage = calculateStorage(params, loads, customData);
  const misc = calculateMisc(storage, loads, customData);
  const summary = calculateSummary(storage, loads, misc, customData, params);

  return {
    specialLoads: loads.specialLoads,
    basicLoads: loads.basicLoads,
    customData,
    computedStorage: storage,
    computedMisc: misc,
    computedSummary: summary,
    meta: {},
  };
}

export default calculate;
