import { DataSeries } from '../domain_model/SourceDataCollection';

const calculateThetaZ = (thetaE: DataSeries): number[] => thetaE.map((x) => 90 - x);

export default calculateThetaZ;
