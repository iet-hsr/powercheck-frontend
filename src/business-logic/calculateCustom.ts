import _ from 'lodash';
import { IParameterCollection } from '../domain_model/Parameters';
import { NR_OF_VALUES } from './constants';
import { DataSeries } from '../domain_model/SourceDataCollection';
import { ICustomData } from '../domain_model/ComputedDataCollection';
import { toEnergy } from '../domain_model/math/helpers';

function calculate(dataSeries: DataSeries, scaling: number) {
  if (dataSeries.length <= 0) {
    return Array(NR_OF_VALUES).fill(0);
  }
  const originalTotalEnergy = toEnergy(
    _.sum(dataSeries) || 0,
  );
  const scalingFactor = scaling / originalTotalEnergy;
  return dataSeries.map((x) => scalingFactor * x);
}

const calculateCustom = (params: IParameterCollection): ICustomData => {
  const {
    customConsumption: {
      dataSeries: consumptionSeries,
      scaling: consumptionScaling,
    },
    customProduction: {
      dataSeries: productionSeries,
      scaling: productionScaling,
    },
  } = params;
  return {
    customConsumption: calculate(consumptionSeries, consumptionScaling),
    customProduction: calculate(productionSeries, productionScaling),
  };
};

export default calculateCustom;
