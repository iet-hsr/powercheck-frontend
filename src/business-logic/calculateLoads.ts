import SourceDataCollection, {
  DataSeries,
  DataSeriesSection,
  IPreloadDataCollection,
} from '../domain_model/SourceDataCollection';
import { IParameterCollection } from '../domain_model/Parameters';
import { ILoads } from '../domain_model/ComputedDataCollection';
import { toEnergy } from '../domain_model/math/helpers';
import { WIND_POWER_CORRECTION_FACTOR } from './constants';


const calculateLoads = (
  srcData: SourceDataCollection,
  preloadSrcData: IPreloadDataCollection,
  params: IParameterCollection,
  solarPreCalc: DataSeries,
): ILoads => {
  const { maxSectionDataCollection, sumSectionDataCollection } = preloadSrcData;

  const endUserOriginalYearlyNeeds = toEnergy(
    sumSectionDataCollection.endUser[params.endUser.yearOrSlug] || 0,
  );

  const importOriginalYearlyNeeds = toEnergy(
    sumSectionDataCollection.import[params.import.yearOrSlug] || 0,
  );

  const exportOriginalYearlyNeeds = toEnergy(
    sumSectionDataCollection.export[params.export.yearOrSlug] || 0,
  );

  const calculateSimpleLoad = (
    section: Exclude<DataSeriesSection, 'dam' | 'solarDiffuseA' | 'solarDirectA' | 'solarDiffuseB'
    | 'solarDirectB' | 'solarDiffuseC' | 'solarDirectC'>,
    getScalingFactor?: (scalingParam: number) => number,
  ) => {
    const { yearOrSlug, scaling } = params[section];
    const dataSeries = srcData.getOneYearDataSeries(section, yearOrSlug);
    const srcMaxVal = maxSectionDataCollection[section][yearOrSlug] || 1;
    const scalingFactor = getScalingFactor
      ? getScalingFactor(scaling)
      : scaling / srcMaxVal;
    return dataSeries.map((dataPoint) => dataPoint * scalingFactor);
  };

  const endUserWithoutEMobility = calculateSimpleLoad(
    'endUser',
    (yearlyNeedsInput) => yearlyNeedsInput / endUserOriginalYearlyNeeds,
  );

  const importCalculated = calculateSimpleLoad(
    'import',
    (yearlyNeedsInput) => yearlyNeedsInput / importOriginalYearlyNeeds,
  );

  const exportCalculated = calculateSimpleLoad(
    'export',
    (yearlyNeedsInput) => yearlyNeedsInput / exportOriginalYearlyNeeds,
  );

  const endUserWithEMobility = endUserWithoutEMobility.map(
    (x) => x + params.endUser.additionalEMobilityEnergy / 365 / 24,
  );

  function calculateSolar() {
    const { scaling } = params.solar;
    return solarPreCalc.map((x) => (
      x * scaling
    ));
  }

  return {
    basicLoads: {
      endUser: endUserWithEMobility,
      import: importCalculated,
      export: exportCalculated,
      nuclear: calculateSimpleLoad('nuclear'),
      thermal: calculateSimpleLoad('thermal'),
      river: calculateSimpleLoad('river'),
      solar: calculateSolar(),
      wind: calculateSimpleLoad(
        'wind',
        (installedPower) => installedPower * WIND_POWER_CORRECTION_FACTOR,
      ),
    },
    specialLoads: {
      networkLosses: [],
      damInflux: srcData.getOneYearDataSeries('dam', params.waterStorage.dam.yearOrSlug),
    },
  };
};

export default calculateLoads;
