import {
  IStorageChargePowers,
  IStorageInputEfficiencies,
  IStorageLevels,
  StorageState,
} from '../types';
import { toEnergy, toPower } from '../../domain_model/math/helpers';

export function charge(
  previousState: StorageState,
  chargePowers: IStorageChargePowers,
  efficiencies: IStorageInputEfficiencies,
  capacities: IStorageLevels,
): StorageState {
  let residualAfter = previousState.residual;
  const levels = { ...previousState.levels };
  const consumedPower: IStorageChargePowers = { ...previousState.consumedPower };

  const chargeSpecificStorage = (type: 'battery' | 'pump') => {
    const { [type]: efficiency } = efficiencies;
    const { [type]: maxChargePower } = chargePowers;
    const { [type]: capacity } = capacities;
    const { [type]: levelBefore } = previousState.levels;

    const availableCapacityInGwh = Math.max(0, capacity - levelBefore);
    // TheoreticalPower is either limited by the free capacity or by the max power parameter.
    const theoreticalInputPowerLimit = Math.min(
      toPower(availableCapacityInGwh) / efficiency,
      maxChargePower - previousState.consumedPower[type],
    );

    // newlyConsumedPower: The actually used power of this charging step
    const newlyConsumedPower = Math.min(theoreticalInputPowerLimit, residualAfter);
    consumedPower[type] += newlyConsumedPower;
    residualAfter -= newlyConsumedPower;

    const newLevelChange = toEnergy(newlyConsumedPower * efficiency);
    // Make sure level is not greater than capacity. (Possibility due to float precision errors)
    levels[type] = Math.min(capacity, levels[type] + newLevelChange);
  };

  chargeSpecificStorage('battery');
  if (residualAfter > 0) {
    chargeSpecificStorage('pump');
  }
  return {
    ...previousState,
    residual: residualAfter,
    levels,
    consumedPower,
  };
}
