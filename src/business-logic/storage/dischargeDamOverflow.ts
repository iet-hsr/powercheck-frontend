import { toEnergy, toPower } from '../../domain_model/math/helpers';
import {
  IStorageLevels,
  IStorageOutputEfficiencies,
  IStoragePowers,
  StorageState,
} from '../types';

export function dischargeDamOverflow(
  previousState: StorageState,
  dischargePowers: IStoragePowers,
  efficiencies: IStorageOutputEfficiencies,
  capacities: IStorageLevels,
): StorageState {
  let requestedPower = -previousState.residual;
  const levels = { ...previousState.levels };
  const producedPower: IStoragePowers = { ...previousState.producedPower };

  const newlyGeneratedPower = Math.min(
    // capacity and level values are both absolute (not usable)
    Math.max(
      0, efficiencies.dam * toPower(levels.dam - capacities.dam),
    ), // Overflow (before efficiency)
    dischargePowers.dam, // max discharge power (after efficiency)
  );
  producedPower.dam += newlyGeneratedPower;
  requestedPower -= newlyGeneratedPower;

  const newLevelChange = toEnergy(-newlyGeneratedPower / efficiencies.dam);
  levels.dam += newLevelChange;

  return {
    ...previousState,
    residual: -requestedPower,
    levels,
    producedPower,
  };
}
