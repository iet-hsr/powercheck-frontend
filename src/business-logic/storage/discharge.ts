import { IStorageOutputEfficiencies, IStoragePowers, StorageState } from '../types';
import { toEnergy, toPower } from '../../domain_model/math/helpers';


export function discharge(
  previousState: StorageState,
  dischargePowers: IStoragePowers,
  efficiencies: IStorageOutputEfficiencies,
): StorageState {
  let requestedPower = -previousState.residual;
  const levels = { ...previousState.levels };
  const producedPower: IStoragePowers = { ...previousState.producedPower };

  const dischargeSpecificStorage = (type: 'battery' | 'pump' | 'dam') => {
    const { [type]: maxDischargePower } = dischargePowers;
    const { [type]: levelBefore } = previousState.levels;
    const { [type]: efficiency } = efficiencies;

    // TheoreticalPower (after efficiency) is either limited by the storage level
    // or by the max power parameter.
    const theoreticalOutputPowerLimit = Math.min(
      efficiency * toPower(levelBefore),
      // maxDischargePower: The user defined max power (after efficiency loss)
      // producedPower: The already generated power (after efficiency loss)
      //                (always 0, except for dam when overflow)
      maxDischargePower - (producedPower[type]),
    );

    // newlyGeneratedPower: The actually generated power of this discharging step
    const newlyGeneratedPower = Math.min(theoreticalOutputPowerLimit, requestedPower);
    producedPower[type] += newlyGeneratedPower;
    requestedPower -= newlyGeneratedPower;

    const newLevelChange = toEnergy(-newlyGeneratedPower / efficiency);
    // Make sure the level is not negative. (Possibility due to float precision errors)
    levels[type] = Math.max(0, levels[type] + newLevelChange);
  };

  dischargeSpecificStorage('battery');
  if (requestedPower > 0) {
    dischargeSpecificStorage('pump');
  }
  if (requestedPower > 0) {
    dischargeSpecificStorage('dam');
  }
  return {
    ...previousState,
    residual: -requestedPower,
    levels,
    producedPower,
  };
}
