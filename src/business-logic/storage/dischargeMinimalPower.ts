import { toEnergy, toPower } from '../../domain_model/math/helpers';
import {
  IMinimalDischargePowers,
  IStorageOutputEfficiencies,
  IStoragePowers,
  StorageState,
} from '../types';

export function dischargeMinimalPower(
  previousState: StorageState,
  minimalDischargePowers: IMinimalDischargePowers,
  efficiencies: IStorageOutputEfficiencies,
): StorageState {
  let requestedPower = -previousState.residual;
  const levels = { ...previousState.levels };
  const producedPower: IStoragePowers = { ...previousState.producedPower };

  const newlyGeneratedPower = Math.min(
    efficiencies.dam * toPower(levels.dam),
    minimalDischargePowers.dam,
  );
  producedPower.dam += newlyGeneratedPower;
  requestedPower -= newlyGeneratedPower;

  const newLevelChange = toEnergy(-newlyGeneratedPower / efficiencies.dam);
  levels.dam += newLevelChange;

  return {
    ...previousState,
    residual: -requestedPower,
    levels,
    producedPower,
  };
}
