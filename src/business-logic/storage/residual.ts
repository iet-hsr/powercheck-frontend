import { ICustomData, ILoads } from '../../domain_model/ComputedDataCollection';

export const calculateResidual = (
  index: number,
  loads: ILoads,
  customData: ICustomData,
): number => {
  const {
    basicLoads: {
      endUser: endUserLoad,
      import: importLoad,
      export: exportLoad,
      solar: solarLoad,
      thermal: thermalLoad,
      nuclear: nuclearLoad,
      river: riverLoad,
      wind: windLoad,
    },
    specialLoads: {
      networkLosses,
    },
  } = loads;

  const {
    customProduction,
    customConsumption,
  } = customData;

  return (
    nuclearLoad[index]
    + thermalLoad[index]
    + solarLoad[index]
    + windLoad[index]
    + riverLoad[index]
    + importLoad[index]
    + (customProduction[index] || 0)
    - endUserLoad[index]
    - networkLosses[index]
    - (customConsumption[index] || 0)
    - exportLoad[index]
  );
};
