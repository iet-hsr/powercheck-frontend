export interface IStorageLevels {
  battery: number;
  pump: number;
  dam: number;
}

export interface IStoragePowers {
  battery: number;
  pump: number;
  dam: number;
}

export interface IMinimalDischargePowers {
  dam: number;
}

export interface IStorageInputEfficiencies {
  battery: number;
  pump: number;
}

export interface IStorageOutputEfficiencies {
  battery: number;
  pump: number;
  dam: number;
}

export interface IStorageChargePowers {
  battery: number;
  pump: number;
}

export type StorageState = {
  residual: number;
  levels: IStorageLevels;
  consumedPower: IStorageChargePowers;
  producedPower: IStoragePowers;
  damOverflow: number;
};
