/* eslint-disable no-param-reassign */
const lowResReducer = (
  partialData: number[],
  val: number,
  index: number,
  data: number[],
): number[] => {
  const DESIRED_NR_OF_SAMPLES = 100;
  const initialNrOfSamples = data.length;

  if (DESIRED_NR_OF_SAMPLES > initialNrOfSamples) {
    partialData[index] = val;
    return partialData;
  }

  const nrOfValuesPerGroup = Math.floor(initialNrOfSamples / DESIRED_NR_OF_SAMPLES);

  const lowResIndexFloat = index / nrOfValuesPerGroup;
  const lowResIndex = +Math.floor(lowResIndexFloat);

  partialData[lowResIndex] = Math.max((partialData[lowResIndex] || 0), val);

  return partialData;
};

export default lowResReducer;
