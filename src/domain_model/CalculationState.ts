enum CalculationState {
  Idle,
  Requested,
  InProgress,
  InProgressNoLoadingScreen,
}

export default CalculationState;
