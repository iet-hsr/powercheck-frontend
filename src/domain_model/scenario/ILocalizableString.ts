export interface ILocalizableString {
  getString: (t: (str: string) => string) => string;
}
