export type Potency = {
  name: string;
  hierarchy: number;
  arrayIndex: number;
}
