import { DataSeries } from './SourceDataCollection';
import { ILocalizableString } from './scenario/ILocalizableString';
import { ScenarioId } from './scenario/Scenario';

export type IComputedDataFlat = IComputedDataWithoutInfoFlat & IScenarioInfo;

export type IComputedData = IComputedDataWithoutInfo & { scenarioInfo: IScenarioInfo; }

export type IComputedDataWithoutInfoFlat
= IBasicLoads
& ISpecialLoads
& ICustomData
& IComputedStorage
& IComputedMisc
& IComputedSummary
& {
  notYetComputed?: boolean;
}

export type IComputedDataWithoutInfo = {
  basicLoads: IBasicLoads;
  specialLoads: ISpecialLoads;
  customData: ICustomData;
  computedStorage: IComputedStorage;
  computedMisc: IComputedMisc;
  computedSummary: IComputedSummary;
  meta: {
    notYetComputed?: boolean;
  };
}

export type SolarLoadPreScalingData = {
  highRes: DataSeries;
  lowRes: DataSeries;
  sum: number;
  max: number;
}

export type SolarLoadPreScalingCacheEntry = {
  hash: string;
  data: SolarLoadPreScalingData;
}

export type IPreComputedData = {
  solar: {
    active: {
      hash: string;
      data: SolarLoadPreScalingData;
    };
    compare: {
      a: SolarComparePreCalcState;
      b: SolarComparePreCalcState;
    };
    cache: SolarLoadPreScalingCacheEntry[];
  };
}

type SolarComparePreCalcState = {
  state: 'CALCULATED';
  data: SolarLoadPreScalingData;
  hash: string;
} | {
  state: 'REQUESTED';
  scenarioId: number;
} | {
  state: 'NOT_INITIALIZED';
}

export type IScenarioInfo = {
  scenarioName: ILocalizableString;
  scenarioDescription: ILocalizableString;
  scenarioId?: ScenarioId;
}

export type IComputedSummary = {
  totalEnergyConsumption: number;
  totalEnergyConsumptionPlusExport: number;
  totalEnergyProduction: number;
  totalEnergyProductionPlusImportAndShortage: number;

  importEnergy: number;
  exportEnergy: number;
  nettImportEnergy: number;

  excessEnergy: number;
  shortageEnergy: number;
  nettShortageEnergy: number;

  selfSufficiencyPercentage: number;
  importDurationH: number;
  importMaxPower: number;

  nuclearEnergyProduction: number;
  thermalEnergyProduction: number;
  solarEnergyProduction: number;
  windEnergyProduction: number;
  riverEnergyProduction: number;
  damEnergyProduction: number;

  pumpEnergyProduction: number;
  pumpEnergyConsumption: number;
  pumpEnergyNettProduction: number;

  batteryEnergyProduction: number;
  batteryEnergyConsumption: number;
  batteryEnergyNettProduction: number;

  customEnergyProduction: number;
  customEnergyConsumption: number;
  customEnergyNettProduction: number;

  networkLossEnergyConsumption: number;
  endUserEnergyConsumption: number;

  usableStorageDifference: number;
  usableFinalStorageLevel: number;
  damUsableStorageLevelEnergyDiff: number;
  damUsableStorageLevelEnergyIncrease: number;
  damUsableStorageLevelEnergyDecrease: number;
  pumpUsableStorageLevelEnergyDiff: number;
  pumpUsableStorageLevelEnergyIncrease: number;
  pumpUsableStorageLevelEnergyDecrease: number;
  batteryUsableStorageLevelEnergyDiff: number;
  batteryUsableStorageLevelEnergyIncrease: number;
  batteryUsableStorageLevelEnergyDecrease: number;
}

export type IComputedMisc = {
  damNegativeProducedPower: DataSeries;
  pumpNegativeProducedPower: DataSeries;
  batteryNegativeProducedPower: DataSeries;

  totalConsumption: DataSeries;
  totalProduction: DataSeries;

  excessSeries: DataSeries;
  shortageSeries: DataSeries;
}

export type IComputedStorage = {
  residual: DataSeries;

  damOverflow: DataSeries;
  damProducedPower: DataSeries;
  damUsableStorageLevel: DataSeries;
  damStorageLevel: DataSeries;

  pumpConsumedPower: DataSeries;
  pumpProducedPower: DataSeries;
  pumpUsableStorageLevel: DataSeries;
  pumpStorageLevel: DataSeries;

  batteryConsumedPower: DataSeries;
  batteryProducedPower: DataSeries;
  batteryUsableStorageLevel: DataSeries;
  batteryStorageLevel: DataSeries;
}

export type ILoads = {
  basicLoads: IBasicLoads;
  specialLoads: ISpecialLoads;
}

export type IBasicLoads = {
  endUser: DataSeries;
  import: DataSeries;
  export: DataSeries;
  nuclear: DataSeries;
  thermal: DataSeries;
  river: DataSeries;
  solar: DataSeries;
  wind: DataSeries;
}

export type ISpecialLoads = {
  networkLosses: DataSeries;
  damInflux: DataSeries;
}

export type ICustomData = {
  customConsumption: DataSeries;
  customProduction: DataSeries;
}
