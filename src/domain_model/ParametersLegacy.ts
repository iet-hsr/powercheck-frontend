import {
  Factor,
  GW,
  GWh,
  Hours,
  IActiveStoragePowers,
  IActiveStoragePowersHours,
  IDataSeries,
  IEMobility,
  IMinimalPower,
  INetwork, IParameterCollectionV9,
  IPassiveStoragePower,
  IPvOrientation,
  IScalable,
  ISolarVersion,
  IStorageEfficiencies,
  IUsableCapacity,
} from './Parameters';

export interface IYearLegacy {
  year: YearLegacy;
}

export type YearLegacy = number;

export type UnknownParameterCollection = IParameterCollectionV0
| IParameterCollectionV1
| IParameterCollectionV2
| IParameterCollectionV3
| IParameterCollectionV4
| IParameterCollectionV5
| IParameterCollectionV6
| IParameterCollectionV7
| IParameterCollectionV8
| IParameterCollectionV9
export interface ICapacityLegacy {
  maxCapacity: GWh;
  initialCapacity: GWh;
}


export interface IPassiveStorageInHoursLegacy extends ICapacityLegacy {
  outputPowerInHours: Hours;
  outputEfficiency: Factor;
}

export interface IActiveStorageHoursLegacy extends IPassiveStorageInHoursLegacy {
  intakePowerInHours: Hours;
  inputEfficiency: Factor;
}

interface IPassiveStorageLegacy extends ICapacityLegacy {
  outputPower: GW;
  outputEfficiency: Factor;
}

interface IActiveStorageLegacy extends IPassiveStorageLegacy {
  intakePower: GW;
  inputEfficiency: Factor;
}

/**
 * Changes:
 *
 * Add minimalDamPower
 */
export interface IParameterCollectionV8 {
  version: 8;
  endUser: IYearLegacy & IScalable & IEMobility;
  import: IYearLegacy & IScalable;
  export: IYearLegacy & IScalable;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable & IPvOrientation & ISolarVersion;
  waterStorage: IStorageEfficiencies & {
    dam: IYearLegacy & IUsableCapacity & IPassiveStoragePower & IMinimalPower
    pumpStorage: IUsableCapacity & IActiveStoragePowers
  }
  batteryStorage: IUsableCapacity & IStorageEfficiencies & IActiveStoragePowersHours;
  network: INetwork;
}

/**
 * Changes:
 * Add import section
 * Add export section
 * Add networkLossFactor
 * Combine dam & pump efficiencies
 * Switch from physical to usable storage capacities
 */
export interface IParameterCollectionV7 {
  version: 7;
  endUser: IYearLegacy & IScalable & IEMobility;
  import: IYearLegacy & IScalable;
  export: IYearLegacy & IScalable;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable & IPvOrientation & ISolarVersion;
  waterStorage: IStorageEfficiencies & {
    dam: IYearLegacy & IUsableCapacity & IPassiveStoragePower
    pumpStorage: IUsableCapacity & IActiveStoragePowers
  }
  batteryStorage: IUsableCapacity & IStorageEfficiencies & IActiveStoragePowersHours;
  network: INetwork;
}

export interface IParameterCollectionV6 {
  version: 6;
  endUser: IYearLegacy & IScalable & IEMobility;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable & IPvOrientation & ISolarVersion;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageHoursLegacy;
}

export interface IParameterCollectionV5 {
  version: 5;
  endUser: IYearLegacy & IScalable & IEMobility;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable & IPvOrientation & ISolarVersion;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}

export interface IParameterCollectionV4 {
  version: 4;
  endUser: IYearLegacy & IScalable & IEMobility;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable & IPvOrientation;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}

export interface IParameterCollectionV3 {
  version: 3;
  endUser: IYearLegacy & IScalable & IEMobility;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}

export interface IParameterCollectionV2 {
  version: 2;
  endUser: IYearLegacy & IScalable;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}

export interface IParameterCollectionV1 {
  endUser: IYearLegacy & IScalable;
  customConsumption: IDataSeries & IScalable;
  customProduction: IDataSeries & IScalable;
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}

export interface IParameterCollectionV0 {
  endUser: IYearLegacy & IScalable;
  customConsumption: number[];
  customProduction: number[];
  nuclear: IYearLegacy & IScalable;
  thermal: IYearLegacy & IScalable;
  wind: IYearLegacy & IScalable;
  river: IYearLegacy & IScalable;
  solar: IYearLegacy & IScalable;
  dam: IYearLegacy & IPassiveStorageLegacy;
  pumpStorage: IActiveStorageLegacy;
  batteryStorage: IActiveStorageLegacy;
}
