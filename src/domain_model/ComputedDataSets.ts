import { IComputedData } from './ComputedDataCollection';

export type ComputedDataSets = { [scenarioId: string]: IComputedData };
